# frozen_string_literal: true

module Resolvers
  module Geo
    class TerraformStateVersionRegistriesResolver < BaseResolver
      include RegistriesResolver
    end
  end
end
